import random


class Ai:
    def move(self, board):
        return random.choice(board.moves())
