import math
from functools import reduce
import time

from .ai import Ai

# Piece Values pawn = 1, knight = 3 ...
values = [1, 3, 3, 5, 9, 0]


class Maple(Ai):
    def __init__(self):
        self.name = 'Maple Heather Bennett-Schafer' # The rabbit
        self.expanded = 0
        self.lookup = {}
        self.lookups_used = 0

    def move(self, board):
        self.expanded = 0
        start = time.time()

        depth = 0
        while time.time() - start < 5:
            depth += 1
            self.lookup = {}
            self.lookups_used = 0

            (score, move) = self.minimax(board, depth, board.turn, True, alpha=-math.inf, beta=math.inf)
            if score > 999:
                break
        end = time.time()
        print("Expanded {} nodes in {} seconds".format(self.expanded, end - start))
        print("Averaged {} per second".format(math.floor(self.expanded / (end - start))))
        score = score if board.turn == 0 else -score
        print("Score: {}".format(score))
        print("Going to depth: {}".format(depth))
        print("Used {} lookups".format(self.lookups_used))

        return move

    def minimax(self, board, depth, color, maximinzing, alpha=-math.inf, beta=math.inf):
        self.expanded += 1

        if depth < 1 or self.terminal_test(board):
            utility = self.utility(board, color)
            return utility, None

        if maximinzing:
            best = -math.inf
            move = None

            for action in board.generate_moves():
                child = board.duplicate()
                child.execute_move(action)

                hashed = hash((child, depth))

                if hashed in self.lookup:
                    self.lookups_used += 1
                    (value, _) = self.lookup[hashed]

                else:
                    new_depth = depth - 1
                    (value, _) = self.minimax(child, new_depth, color, False, alpha=alpha, beta=beta)

                    self.lookup[hashed] = value, action

                if value > best:
                    move = action
                    best = value

                if value >= beta:
                    return best, move

                alpha = max(alpha, value)
            return best, move

        else:
            best = math.inf
            move = None

            for action in board.generate_moves():
                child = board.duplicate()
                child.execute_move(action)

                hashed = hash((child, depth))

                if hashed in self.lookup:
                    self.lookups_used += 1
                    (value, _) = self.lookup[hashed]

                else:
                    new_depth = depth - 1
                    (value, _) = self.minimax(child, new_depth, color, True, alpha=alpha, beta=beta)

                    self.lookup[hashed] = value, action

                if value < best:
                    move = action
                    best = value

                if value <= alpha:
                    return best, move

                beta = min(beta, value)

            return best, move

    @staticmethod
    def terminal_test(board):
        kings = [piece for piece in board.pieces if piece.type == 5]

        return len(kings) != 2

    def utility(self, board, color):
        if self.terminal_test(board):
            king = [piece for piece in board.pieces if piece.type == 5]
            return math.inf if king[0].color == color else -math.inf

        def piece_value(total, piece):
            sign = 1 if piece.color == color else -1
            return sign * values[piece.type] + total
        return reduce(piece_value, board.pieces, 0)