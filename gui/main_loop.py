from tkinter import *
import os
import math
from ai import Maple
dir = os.path.dirname(__file__)

sprites_to_piece_type = [5, 3, 4, 2, 0, 1]

class App(Frame):
    def __init__(self, parent, board, square_size=80, board_size=8):
        self.board = board
        self.canvas_size = square_size * board_size
        self.square_mid = square_size / 2
        self.board_size = board_size
        self.square_size = square_size
        self.canvas = None
        self.from_square = None
        self.to_square = None
        self.maple = Maple()
        self.players = ['Player', 'ai']

        self.frame = Frame(parent)

        Label(self.frame, text="Maple Heather Bennett-Schafer").grid(row=0)

        sprite = PhotoImage(file=os.path.join(dir, "../static_content/ChessPiecesArray.png"))
        self.sprites = self.extract_sprite(sprite, 6, 2, 60)

        Label(self.frame, text="Banner Brummitt Schafer").grid(row=2)

        self.frame.pack()

    def draw_board(self):
        board = self.board
        self.canvas = Canvas(self.frame, width=self.canvas_size, height=self.canvas_size)

        for row in range(self.board_size):
            for col in range(self.board_size):
                start_x = row * self.square_size
                start_y = col * self.square_size
                fill = "#E7E9FE" if (row + col) % 2 == 0 else "#111C6A"
                self.canvas.create_rectangle(start_x, start_y, start_x + self.square_size, start_y + self.square_size,
                                             fill=fill)

        for piece in board.pieces:
            number = sprites_to_piece_type[piece.type] * 2
            number = number + 1 if piece.color == 0 else number
            self.canvas.create_image(self.square_to_pixel(piece.square), image=self.sprites[number])

        self.canvas.grid(row=1)
        self.canvas.bind("<Button-1>", self.callback)

    def callback(self, event):
        if self.from_square is None:
            self.from_square = self.pixel_to_square((event.x, event.y))
        else:
            to_square = self.pixel_to_square((event.x, event.y))
            self.action(move=(self.from_square, to_square, None))

    def action(self, move=None):
        if self.players[self.board.turn] == 'ai':
            print('here')
            move = self.maple.move(self.board)
            print(move)
            self.board.make_move(move)

        elif move is not None:
            try:
                self.board.make_move(move)
            finally:
                self.from_square = None

        self.draw_board()
        self.action()

    def square_to_pixel(self, square):
        x = (self.square_size * (square[0]) + self.square_mid)
        y = self.square_size * self.board_size - (self.square_size * (square[1]) + self.square_mid)

        return x, y

    def pixel_to_square(self, coordinate):
        x = math.floor(coordinate[0] / self.square_size)
        y = self.board_size - math.floor(coordinate[1] / self.square_size) - 1

        return x, y

    @staticmethod
    def extract_sprite(sprite, x, y, d):
        return [App.subimage(sprite, d * i, d * j, d * (i + 1), d * (j + 1)) for i in range(x) for j in range(y)]

    @staticmethod
    def subimage(spritesheet, l, t, r, b):
        dst = PhotoImage()
        dst.tk.call(dst, 'copy', spritesheet, '-from', l, t, r, b, '-to', 0, 0)
        return dst


def main_loop(board, seq=None):
    root = Tk()
    root.title("Chess")
    app = App(root, board)
    app.draw_board()

    root.mainloop()


if __name__ == '__main__':
    main_loop()
