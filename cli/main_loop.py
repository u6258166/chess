from .square_converter import square_to_num, num_to_square
from ai import Maple
import re


def main_loop(board, seq=""):
    i = 0
    seq = seq.split(',')
    maple = Maple()
    players = ['cli', 'cli']

    print(board)

    while True:
        if players[board.turn] == 'ai':
            print('Thinking...')
            move = maple.move(board)
            board.make_move(move)

        else:
            action = input('Please make a move: ') if len(seq) < i + 1 else seq[i]
            i += 1

            if action == 'q' or action == 'quit':
                break

            if action == 'h' or action == 'help':
                human_readable_moves = [num_to_square(move) for move in board.moves()]
                print(', '.join(human_readable_moves))
                continue

            if re.search('h\s[a-h][1-8]', action):
                square = square_to_num(action.split()[1])
                human_readable_moves = [num_to_square(move) for move in board.moves() if move[0] == square]
                print(', '.join(human_readable_moves))
                continue

            else:
                parts = action.split()
                try:
                    validate_input(parts)
                    parts2 = parts[2] if len(parts) == 3 else None
                    square = (square_to_num(parts[0]), square_to_num(parts[1]), parts2)
                    board.make_move(square)
                except ValueError as error:
                    print(error)
                    continue

        print(board)


def validate_input(parts):
    if len(parts) < 2:
        raise ValueError('Not enough squares given')

    if len(parts) > 3:
        raise ValueError('Too many inputs given')

    if len(parts[0]) != 2:
        raise ValueError('Initial square incorrectly formatted')

    if len(parts[1]) != 2:
        raise ValueError('Destination square incorrectly formatted')
