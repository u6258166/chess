lookup = 'abcdefgh'


def square_to_num(square):
    return (lookup.index(square[0]), int(square[1]) - 1)


def num_to_square(num):
    return "({}{}, {}{})".format(lookup[num[0][0]], num[0][1] + 1, lookup[num[1][0]], num[1][1] + 1)
