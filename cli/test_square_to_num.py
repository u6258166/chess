from unittest import TestCase
from cli.square_converter import square_to_num


class TestSquareToNum(TestCase):
    def test_square_to_num(self):
        self.assertEqual(square_to_num('a1'), (0, 0))
        self.assertEqual(square_to_num('b2'), (1, 1))
        self.assertEqual(square_to_num('e4'), (4, 3))
        self.assertEqual(square_to_num('a8'), (0, 7))
        self.assertEqual(square_to_num('h1'), (7, 0))
        self.assertEqual(square_to_num('h8'), (7, 7))