from .piece import Piece


class Queen(Piece):
    def __str__(self):
        return '♕' if self.color == 1 else '♛'

    def moves(self, board):
        return super().straight(board).union(super().diagonal(board))
