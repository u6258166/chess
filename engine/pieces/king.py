from .piece import Piece


class King(Piece):
    def __str__(self):
        return '♔' if self.color == 1 else '♚'

    def moves(self, board):
        return super().straight(board, limit=1).union(super().diagonal(board, limit=1))
