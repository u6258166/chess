import math


class Piece:
    def __init__(self, raw_piece):
        self.square = raw_piece[2][0], raw_piece[2][1]
        self.color = raw_piece[1]
        self.type = raw_piece[0]
        self.has_moved = True if len(raw_piece) == 4 and raw_piece[3] else False

    def __str__(self):
        return '🐯'

    def __hash__(self):
        return hash((self.square, self.color, self.type))

    def rep(self):
        return [self.type, self.color, (self.square[0], self.square[1]), self.has_moved]

    def moves(self, board):
        return set()

    def straight(self, board, limit=math.inf):
        directions = [
            (1, 0),
            (0, 1),
            (-1, 0),
            (0, -1)
        ]

        return self.expand_in_directions(board, directions, limit)

    def diagonal(self, board, limit=math.inf):
        directions = [
            (1, 1),
            (1, -1),
            (-1, 1),
            (-1, -1)
        ]

        return self.expand_in_directions(board, directions, limit)

    def expand_in_directions(self, board, directions, limit):
        moves = set()

        x = self.square[0]
        y = self.square[1]

        for (nx, ny) in directions:

            i = 0
            while i < limit:
                i += 1
                potential_move = (x + nx * i, y + ny * i)

                if potential_move[0] > 7 or potential_move[0] < 0 or potential_move[1] > 7 or potential_move[1] < 0:
                    break

                piece_on_destination = board.piece_on(potential_move)

                if piece_on_destination and piece_on_destination.color == self.color:
                    break

                if piece_on_destination and not piece_on_destination.color == self.color:
                    moves.add((self.square, potential_move, None))
                    break

                else:
                    moves.add((self.square, potential_move, None))

        return moves
