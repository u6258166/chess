from .piece import Piece


class Bishop(Piece):
    def __str__(self):
        return '♗' if self.color == 1 else '♝'

    def moves(self, board):
        return super().diagonal(board)
