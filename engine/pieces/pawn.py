from .piece import Piece


class Pawn(Piece):
    def __str__(self):
        return '♟' if self.color == 0 else '♙'

    def moves(self, board):
        direction = 1 if self.color == 0 else -1

        # Straight Moves
        spaces = 1 if self.has_moved else 2

        moves = self.expand_in_directions(board, [(0, direction)], spaces)

        # Capturing Moves
        directions = [(1, direction), (-1, direction)]
        for (dx, dy) in directions:
            new_x = self.square[0] + dx
            new_y = self.square[1] + dy

            piece = board.piece_on((new_x, new_y))

            if piece and piece.color != self.color:
                moves.add((self.square, (new_x, new_y), None))

        return moves
