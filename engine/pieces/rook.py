from .piece import Piece


class Rook(Piece):
    def __str__(self):
        return '♖' if self.color == 1 else '♜'

    def moves(self, board):
        return super().straight(board)
