from .piece import Piece


class Knight(Piece):
    def __str__(self):
        return '♘' if self.color == 1 else '♞'

    def moves(self, board):
        directions = [[2, 1], [1, 2], [-1, 2], [2, -1], [-2, 1], [1, -2], [-1, -2], [-2, -1]]

        potential_moves = []

        for direction in directions:
            x = self.square[0] + direction[0]
            y = self.square[1] + direction[1]

            piece = board.piece_on((x, y))

            if piece and piece.color == self.color:
                continue

            if 0 <= x < 8 and 0 <= y < 8:
                potential_moves.append((self.square, (x, y), None))

        return potential_moves
