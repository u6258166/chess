import hashlib

from .initialize_pieces import initialize_pieces
from .validators import validate_move

BOARD_SIZE = 8
INITIAL_POSITION = {
    'pieces': [
        # Rooks
        [3, 0, (0, 0)], [3, 0, (7, 0)], [3, 1, (0, 7)], [3, 1, (7, 7)],
        # Kings
        [5, 0, (4, 0)], [5, 1, (4, 7)],
        # Bishops
        [2, 0, (2, 0)], [2, 0, (5, 0)], [2, 1, (2, 7)], [2, 1, (5, 7)],
        # Knights
        [1, 0, (1, 0)], [1, 0, (6, 0)], [1, 1, (1, 7)], [1, 1, (6, 7)],
        # Queens
        [4, 0, (3, 0)], [4, 1, (3, 7)],
        # White Pawns
        [0, 0, (0, 1)], [0, 0, (1, 1)], [0, 0, (2, 1)], [0, 0, (3, 1)],
        [0, 0, (4, 1)], [0, 0, (5, 1)], [0, 0, (6, 1)], [0, 0, (7, 1)],
        # Black Pawns
        [0, 1, (0, 6)], [0, 1, (1, 6)], [0, 1, (2, 6)], [0, 1, (3, 6)],
        [0, 1, (4, 6)], [0, 1, (5, 6)], [0, 1, (6, 6)], [0, 1, (7, 6)],
    ],
    'last_move': None,
    'history': [],
    'turn': 0
}


class Board:
    def __init__(self, position=INITIAL_POSITION):
        self.pieces = initialize_pieces(position['pieces'])
        self.last_move = position['last_move']
        self.history = position['history']
        self.turn = position['turn']

    def __str__(self):
        stringified = ''

        for col in reversed(range(BOARD_SIZE)):
            for row in range(BOARD_SIZE):
                piece = self.piece_on((row, col))
                if piece:
                    stringified += ' ' + piece.__str__() + ' '
                else:
                    stringified += '   '
            stringified += "\n\n"

        return stringified

    def __hash__(self):
        return hash((frozenset([hash(piece) for piece in self.pieces]), self.turn))

    def make_move(self, move):
        validate_move(move, self)
        if move in self.moves():
            self.execute_move(move)
        else:
            raise ValueError("Illegal Move")

    def execute_move(self, move):
        piece = self.piece_on(move[0])
        opponent_piece = self.piece_on(move[1])

        if opponent_piece:
            self.pieces.remove(opponent_piece)

        piece.square = move[1]
        piece.has_moved = True
        self.turn = 0 if self.turn == 1 else 1

    def piece_on(self, square):
        return next((piece for piece in self.pieces if piece.square[0] == square[0] and piece.square[1] == square[1])
                    , None)

    def king(self, color):
        return next((piece for piece in self.pieces if piece.color == color and piece.type == 5), None)

    def generate_moves(self):
        pieces = [piece for piece in self.pieces if piece.color == self.turn]
        moves = [piece.moves(self) for piece in pieces]

        return [val for sublist in moves for val in sublist]

    def moves(self):
        flattened = set(self.generate_moves())

        # Remove moves that put the king in check
        bad_moves = set()

        turn = self.turn
        for move in flattened:
            alternate_board = self.duplicate()
            alternate_board.execute_move(move)
            king = alternate_board.king(turn)
            for attack in alternate_board.generate_moves():
                if attack[1] == king.square:
                    bad_moves.add(move)
                    break

        return list(flattened - bad_moves)

    def duplicate(self):
        return Board({
            'pieces': [piece.rep() for piece in self.pieces],
            'last_move': self.last_move,
            'history': self.history,
            'turn': self.turn
        })
