def validate_move(move, board):
    if move == (0, 0, 0):
        return True

    if len(move) > 3:
        raise ValueError('Invalid move input')

    if move[0][0] > 7 or move[0][0] < 0:
        raise ValueError("Move out of board range")

    if move[0][1] > 7 or move[0][1] < 0:
        raise ValueError("Move out of board range")

    if move[1][0] > 7 or move[1][0] < 0:
        raise ValueError("Move out of board range")

    if move[0][1] > 7 or move[0][1] < 0:
        raise ValueError("Move out of board range")

    if not board.piece_on(move[0]):
        raise ValueError("No piece on square")

    if board.piece_on(move[0]).color != board.turn:
        raise ValueError("Cannot move opponents piece")

    if move[2] is not None and move[2] not in 'nbrq':
        raise ValueError("Promotions must be made with the letter corresponding to the piece to promote to.\n"
                         "n - Knight, b - Bishop, r - Rook, q - Queen")

    return True
