from .pieces.rook import Rook
from .pieces.king import King
from .pieces.bishop import Bishop
from .pieces.queen import Queen
from .pieces.knight import Knight
from .pieces.pawn import Pawn

pieceMap = [Pawn, Knight, Bishop, Rook, Queen, King]


def initialize_pieces(raw_pieces):
    return [create_piece(piece) for piece in raw_pieces]


def create_piece(piece):
    piece_type = piece[0]
    return pieceMap[piece_type](piece)
