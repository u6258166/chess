from engine import Board
import cli
import gui
import sys


def main():
    board = Board()

    seq = sys.argv[1] if len(sys.argv) > 1 else ""
    result = cli.main_loop(board, seq=seq)
    # result = gui.main_loop(board, seq=seq)

    print('Ending Game')

if __name__ == '__main__':
    main()