# Chess

> Chess game for implementing adversarial search techniques

## Deprecation Notice

This engine works well as a proof of concept tool however it generally achieves
approximately 10k nodes per second. With alpha-beta pruning and look up tables
this allows the engine to look up to 5 plys in advanced using naieve huristics.
This is not enough to beat me so I have been looking at ways to preform speed

I am starting a new project written in rust-lang using bitboard representation
and letting the ai handle checkmates. Preliminary tests indicate that this
will allow the engine to search 3m nodes per second. If you would like to 
contribute to either project please do.

[rusty-chess](https://gitlab.cecs.anu.edu.au/u6258166/rusty-chess)


## Todo

- Document
- Finish engine
    - Castling
    - En Passant
    - Pawn Promotions
    - Knights moving to squares with other pieces
    - Winning callback
    - Drawing
        - Three move
        - Stalemate
        - Resigning
- Stringify for debugging
- Improve AI
    - Singular extensions
    - Monte carlo sampling
- Unit Testing / CI / CD
    
## Development

Developments accepted by merge requests to develop branch.

## Running

```bash
python main.py
```

Alternatively, you can run with a sequence of commands attached. This is helpful for
profiling because all  human moves can be made without waiting on input character
strings.

```bash
# Will play d2 d4 on the first move and c2 c4 on the second move
# regardless of blacks response.
# This assumes an AI is playing for black.
python main.py "d2 d4,c2 c4,q"
```
